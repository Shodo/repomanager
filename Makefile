include .env

build:
	if [ -f "${BINARY}" ]; then \
		rm ./bin/${BINARY}; \
		echo "DELETED ${BINARY}"; \
	fi
	@echo "BUILDING BINARY" 
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o ./bin/${BINARY} ./cmd/app/*.go 

run: build
	./bin/${BINARY} 
	@echo "SERVER STARTED" 

stop:
	@echo "STOPPING SERVER" 
	@pkill -SIGTERM -f "./bin/${BINARY}" 
	@echo "SERVER STOPPED"