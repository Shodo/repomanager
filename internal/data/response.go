package data

type Response struct {
	Error   bool        `json:"error"`
	Message string      `json:"message"`
	Buffer  interface{} `json:"data,omitempty"`
}
