package gateway

import (
	"shodo/repomanager/internal"

	"code.gitea.io/sdk/gitea"
)

type IRepositoryGateway interface {
	CreateRepository(gitea.CreateRepoOption) (*gitea.Repository, *gitea.Response, error)
	GetRepository(string, string) (*gitea.Repository, *gitea.Response, error)
	EditRepository(string, string, gitea.EditRepoOption) (*gitea.Repository, *gitea.Response, error)
	DeleteRepository(string, string) (*gitea.Response, error)
	ListRepositoryBranches(string, string, gitea.ListRepoBranchesOptions) ([]*gitea.Branch, *gitea.Response, error)
	GetSpecificBranch(string, string, string) (*gitea.Branch, *gitea.Response, error)
	ListRepositoryCollaborators(string, string, gitea.ListCollaboratorsOptions) ([]*gitea.User, *gitea.Response, error)
	CreateBranch(string, string, gitea.CreateBranchOption) (*gitea.Branch, *gitea.Response, error)
	DeleteBranch(string, string, string) (bool, *gitea.Response, error)
	CreateBranchProtection(string, string, gitea.CreateBranchProtectionOption) (*gitea.BranchProtection, *gitea.Response, error)
	EditBranchProtection(string, string, string, gitea.EditBranchProtectionOption) (*gitea.BranchProtection, *gitea.Response, error)
	DeleteBranchProtection(string, string, string) (*gitea.Response, error)
	AddCollaboratorToRepository(string, string, string, gitea.AddCollaboratorOption) (*gitea.Response, error)
	DeleteCollaboratorFromRepository(string, string, string) (*gitea.Response, error)
}

type RepositoryGateway struct {
	c *gitea.Client
}

func NewRepositoryGateway() *RepositoryGateway {
	return &RepositoryGateway{
		c: internal.GetGiteaClientInstance(),
	}
}

func (o *RepositoryGateway) CreateRepository(option gitea.CreateRepoOption) (*gitea.Repository, *gitea.Response, error) {
	repo, r, err := o.c.CreateRepo(option)

	if err != nil {
		return nil, nil, err
	}

	return repo, r, nil
}

func (o *RepositoryGateway) GetRepository(owner, repoName string) (*gitea.Repository, *gitea.Response, error) {
	repo, r, err := o.c.GetRepo(owner, repoName)

	if err != nil {
		return nil, nil, err
	}

	return repo, r, nil
}

func (o *RepositoryGateway) EditRepository(owner, repoName string, option gitea.EditRepoOption) (*gitea.Repository, *gitea.Response, error) {
	repo, r, err := o.c.EditRepo(owner, repoName, option)

	if err != nil {
		return nil, nil, err
	}

	return repo, r, nil
}

func (o *RepositoryGateway) DeleteRepository(owner, repoName string) (*gitea.Response, error) {
	r, err := o.c.DeleteRepo(owner, repoName)

	if err != nil {
		return nil, err
	}

	return r, nil
}

func (o *RepositoryGateway) ListRepositoryBranches(user string, repo string, option gitea.ListRepoBranchesOptions) ([]*gitea.Branch, *gitea.Response, error) {
	b, r, err := o.c.ListRepoBranches(user, repo, option)

	if err != nil {
		return nil, nil, err
	}

	return b, r, nil
}

func (o *RepositoryGateway) GetSpecificBranch(user string, repoName string, branch string) (*gitea.Branch, *gitea.Response, error) {
	b, r, err := o.c.GetRepoBranch(user, repoName, branch)

	if err != nil {
		return nil, nil, err
	}

	return b, r, nil
}

func (o *RepositoryGateway) ListRepositoryCollaborators(user string, repoName string, option gitea.ListCollaboratorsOptions) ([]*gitea.User, *gitea.Response, error) {
	c, r, err := o.c.ListCollaborators(user, repoName, option)

	if err != nil {
		return nil, nil, err
	}

	return c, r, nil
}

func (o *RepositoryGateway) CreateBranch(owner string, repo string, option gitea.CreateBranchOption) (*gitea.Branch, *gitea.Response, error) {
	b, r, err := o.c.CreateBranch(owner, repo, option)

	if err != nil {
		return nil, nil, err
	}

	return b, r, nil
}

func (o *RepositoryGateway) DeleteBranch(owner string, repo string, branch string) (bool, *gitea.Response, error) {
	b, r, err := o.c.DeleteRepoBranch(owner, repo, branch)

	if err != nil {
		return false, nil, err
	}

	return b, r, nil
}

func (o *RepositoryGateway) CreateBranchProtection(owner string, repo string, option gitea.CreateBranchProtectionOption) (*gitea.BranchProtection, *gitea.Response, error) {
	bProtection, r, err := o.c.CreateBranchProtection(owner, repo, option)

	if err != nil {
		return nil, nil, err
	}

	return bProtection, r, nil
}

func (o *RepositoryGateway) EditBranchProtection(owner string, repo string, name string, option gitea.EditBranchProtectionOption) (*gitea.BranchProtection, *gitea.Response, error) {
	bProtection, r, err := o.c.EditBranchProtection(owner, repo, name, option)

	if err != nil {
		return nil, nil, err
	}

	return bProtection, r, nil
}

func (o *RepositoryGateway) DeleteBranchProtection(owner string, repo string, name string) (*gitea.Response, error) {
	r, err := o.c.DeleteBranchProtection(owner, repo, name)

	if err != nil {
		return nil, err
	}

	return r, nil
}

func (o *RepositoryGateway) AddCollaboratorToRepository(user string, repo string, collaborator string, option gitea.AddCollaboratorOption) (*gitea.Response, error) {
	r, err := o.c.AddCollaborator(user, repo, collaborator, option)

	if err != nil {
		return nil, err
	}

	return r, nil
}

func (o *RepositoryGateway) DeleteCollaboratorFromRepository(user string, repo string, collaborator string) (*gitea.Response, error) {
	r, err := o.c.DeleteCollaborator(user, repo, collaborator)

	if err != nil {
		return nil, err
	}

	return r, nil
}
