package gateway

import (
	"shodo/repomanager/internal"

	"code.gitea.io/sdk/gitea"
)

type IOrganizationGateway interface {
	CreateOrganization(gitea.CreateOrgOption) (*gitea.Organization, *gitea.Response, error)
	GetOrganization(string) (*gitea.Organization, *gitea.Response, error)
	EditOrganization(string, gitea.EditOrgOption) (*gitea.Response, error)
	DeleteOrganization(string) (*gitea.Response, error)
	ListOrganizationMembers(string, gitea.ListOrgMembershipOption) ([]*gitea.User, *gitea.Response, error)
	CreateRepositoryInOrganization(string, gitea.CreateRepoOption) (*gitea.Repository, *gitea.Response, error)
	CreateTeam(string, gitea.CreateTeamOption) (*gitea.Team, *gitea.Response, error)
	DeleteTeam(int64) (*gitea.Response, error)
	AddTeamMember(int64, string) (*gitea.Response, error)
	RemoveTeamMember(int64, string) (*gitea.Response, error)
	AddRepositoryToTeam(int64, string, string) (*gitea.Response, error)
}

type OrganizationGateway struct {
	c *gitea.Client
}

func NewOrganizationGateway() *OrganizationGateway {
	return &OrganizationGateway{
		c: internal.GetGiteaClientInstance(),
	}
}

func (o *OrganizationGateway) CreateOrganization(option gitea.CreateOrgOption) (*gitea.Organization, *gitea.Response, error) {
	org, r, err := o.c.CreateOrg(option)

	if err != nil {
		return nil, nil, err
	}

	return org, r, nil
}

func (o *OrganizationGateway) GetOrganization(orgName string) (*gitea.Organization, *gitea.Response, error) {
	org, r, err := o.c.GetOrg(orgName)

	if err != nil {
		return nil, nil, err
	}

	return org, r, nil
}

func (o *OrganizationGateway) EditOrganization(orgName string, option gitea.EditOrgOption) (*gitea.Response, error) {
	r, err := o.c.EditOrg(orgName, option)

	if err != nil {
		return nil, err
	}

	return r, nil
}

func (o *OrganizationGateway) DeleteOrganization(orgName string) (*gitea.Response, error) {
	r, err := o.c.DeleteOrg(orgName)

	if err != nil {
		return nil, err
	}

	return r, nil
}

func (o *OrganizationGateway) ListOrganizationMembers(orgName string, option gitea.ListOrgMembershipOption) ([]*gitea.User, *gitea.Response, error) {
	users, r, err := o.c.ListOrgMembership(orgName, option)

	if err != nil {
		return nil, nil, err
	}

	return users, r, nil
}

func (o *OrganizationGateway) CreateRepositoryInOrganization(orgName string, option gitea.CreateRepoOption) (*gitea.Repository, *gitea.Response, error) {
	repo, r, err := o.c.CreateOrgRepo(orgName, option)

	if err != nil {
		return nil, nil, err
	}

	return repo, r, nil
}

func (o *OrganizationGateway) CreateTeam(orgName string, option gitea.CreateTeamOption) (*gitea.Team, *gitea.Response, error) {
	team, r, err := o.c.CreateTeam(orgName, option)

	if err != nil {
		return nil, nil, err
	}

	return team, r, nil
}

func (o *OrganizationGateway) DeleteTeam(teamId int64) (*gitea.Response, error) {
	r, err := o.c.DeleteTeam(teamId)

	if err != nil {
		return nil, err
	}

	return r, nil
}

func (o *OrganizationGateway) AddTeamMember(id int64, user string) (*gitea.Response, error) {
	r, err := o.c.AddTeamMember(id, user)

	if err != nil {
		return nil, err
	}

	return r, nil
}

func (o *OrganizationGateway) RemoveTeamMember(id int64, user string) (*gitea.Response, error) {
	r, err := o.c.RemoveTeamMember(id, user)

	if err != nil {
		return nil, err
	}

	return r, nil
}

func (o *OrganizationGateway) AddRepositoryToTeam(id int64, org, repo string) (*gitea.Response, error) {
	r, err := o.c.AddTeamRepository(id, org, repo)

	if err != nil {
		return nil, err
	}

	return r, nil
}
