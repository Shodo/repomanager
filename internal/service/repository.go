package service

import (
	"net/http"
	"shodo/repomanager/internal/data"
	"shodo/repomanager/internal/gateway"

	"code.gitea.io/sdk/gitea"
)

type RepositoryService struct {
	repository gateway.IRepositoryGateway
}

func NewRepositoryService() *RepositoryService {
	return &RepositoryService{
		repository: gateway.NewRepositoryGateway(),
	}
}

func (o *RepositoryService) CreateRepository(option *data.CreateRepoOption) (*data.Response, error) {
	r, _, err := o.repository.CreateRepository(gitea.CreateRepoOption{
		Name:          option.Name,
		Description:   option.Description,
		Private:       option.Private,
		IssueLabels:   option.IssueLabels,
		AutoInit:      option.AutoInit,
		Template:      option.Template,
		Gitignores:    option.Gitignores,
		License:       option.License,
		Readme:        option.Readme,
		DefaultBranch: option.DefaultBranch,
		TrustModel:    gitea.TrustModel(option.TrustModel),
	})

	if err != nil {
		return nil, err
	}

	return &data.Response{
		Error:   false,
		Message: "success",
		Buffer:  r,
	}, nil
}

func (o *RepositoryService) GetRepository(owner, repo string) (*data.Response, error) {
	r, _, err := o.repository.GetRepository(owner, repo)

	if err != nil {
		return nil, err
	}

	return &data.Response{
		Error:   false,
		Message: "success",
		Buffer:  r,
	}, nil
}

func (o *RepositoryService) EditRepository(owner string, repoName string, option *data.EditRepoOption) (*data.Response, error) {
	repo, _, err := o.repository.EditRepository(owner, repoName, gitea.EditRepoOption{
		Name:                      option.Name,
		Description:               option.Description,
		Private:                   option.Private,
		Template:                  option.Template,
		HasIssues:                 option.HasIssues,
		InternalTracker:           (*gitea.InternalTracker)(option.InternalTracker),
		ExternalTracker:           (*gitea.ExternalTracker)(option.ExternalTracker),
		HasWiki:                   option.HasWiki,
		ExternalWiki:              (*gitea.ExternalWiki)(option.ExternalWiki),
		DefaultBranch:             option.DefaultBranch,
		HasPullRequests:           option.HasPullRequests,
		HasProjects:               option.HasProjects,
		IgnoreWhitespaceConflicts: option.IgnoreWhitespaceConflicts,
		AllowMerge:                option.AllowMerge,
		AllowRebase:               option.AllowRebase,
		AllowRebaseMerge:          option.AllowRebaseMerge,
		AllowSquash:               option.AllowSquash,
		Archived:                  option.Archived,
		MirrorInterval:            option.MirrorInterval,
		AllowManualMerge:          option.AllowManualMerge,
		AutodetectManualMerge:     option.AutodetectManualMerge,
		DefaultMergeStyle:         (*gitea.MergeStyle)(option.DefaultMergeStyle),
	})

	if err != nil {
		return nil, err
	}

	return &data.Response{
		Error:   false,
		Message: "success",
		Buffer:  repo,
	}, nil
}

func (o *RepositoryService) DeleteRepository(owner, repoName string) (*http.Response, error) {
	r, err := o.repository.DeleteRepository(owner, repoName)

	if err != nil {
		return nil, err
	}

	return r.Response, nil
}

func (o *RepositoryService) ListRepositoryBranches(user string, repo string, option *data.ListRepoBranchesOptions) (*data.Response, error) {
	r, _, err := o.repository.ListRepositoryBranches(user, repo, gitea.ListRepoBranchesOptions{
		ListOptions: gitea.ListOptions{
			Page:     option.Page,
			PageSize: option.PageSize,
		},
	})

	if err != nil {
		return nil, err
	}

	return &data.Response{
		Error:   false,
		Message: "success",
		Buffer:  r,
	}, nil
}

func (o *RepositoryService) GetSpecificBranch(user, repoName, branch string) (*data.Response, error) {
	team, r, err := o.repository.GetSpecificBranch(user, repoName, branch)

	if err != nil {
		return nil, err
	}

	return &data.Response{
		Error:   false,
		Message: r.Status,
		Buffer:  team,
	}, nil
}

func (o *RepositoryService) ListRepositoryCollaborators(user string, repoName string, option *data.ListCollaboratorsOptions) (*data.Response, error) {
	cList, _, err := o.repository.ListRepositoryCollaborators(user, repoName, gitea.ListCollaboratorsOptions{
		ListOptions: gitea.ListOptions{
			Page:     option.Page,
			PageSize: option.PageSize,
		},
	})

	if err != nil {
		return nil, err
	}

	return &data.Response{
		Error:   false,
		Message: "success",
		Buffer:  cList,
	}, nil
}

func (o *RepositoryService) CreateBranch(owner string, repo string, option *data.CreateBranchOption) (*data.Response, error) {
	b, _, err := o.repository.CreateBranch(owner, repo, gitea.CreateBranchOption{
		BranchName:    option.BranchName,
		OldBranchName: option.OldBranchName,
	})

	if err != nil {
		return nil, err
	}

	return &data.Response{
		Error:   false,
		Message: "success",
		Buffer:  b,
	}, nil
}

func (o *RepositoryService) DeleteBranch(owner, repo, branch string) (*data.Response, error) {
	b, _, err := o.repository.DeleteBranch(owner, repo, branch)

	if err != nil {
		return nil, err
	}

	return &data.Response{
		Error:   false,
		Message: "success",
		Buffer:  b,
	}, nil
}

func (o *RepositoryService) CreateBranchProtection(owner string, repo string, option *data.CreateBranchProtectionOption) (*data.Response, error) {
	b, _, err := o.repository.CreateBranchProtection(owner, repo, gitea.CreateBranchProtectionOption{
		BranchName:                    option.BranchName,
		EnablePush:                    option.EnablePush,
		EnablePushWhitelist:           option.EnablePushWhitelist,
		PushWhitelistUsernames:        option.PushWhitelistUsernames,
		PushWhitelistTeams:            option.PushWhitelistTeams,
		PushWhitelistDeployKeys:       option.PushWhitelistDeployKeys,
		EnableMergeWhitelist:          option.EnableMergeWhitelist,
		MergeWhitelistUsernames:       option.MergeWhitelistUsernames,
		MergeWhitelistTeams:           option.MergeWhitelistTeams,
		EnableStatusCheck:             option.EnableStatusCheck,
		StatusCheckContexts:           option.StatusCheckContexts,
		RequiredApprovals:             option.RequiredApprovals,
		EnableApprovalsWhitelist:      option.EnableApprovalsWhitelist,
		ApprovalsWhitelistUsernames:   option.ApprovalsWhitelistUsernames,
		ApprovalsWhitelistTeams:       option.ApprovalsWhitelistTeams,
		BlockOnRejectedReviews:        option.BlockOnRejectedReviews,
		BlockOnOfficialReviewRequests: option.BlockOnOfficialReviewRequests,
		BlockOnOutdatedBranch:         option.BlockOnOutdatedBranch,
		DismissStaleApprovals:         option.DismissStaleApprovals,
		RequireSignedCommits:          option.RequireSignedCommits,
		ProtectedFilePatterns:         option.ProtectedFilePatterns,
	})

	if err != nil {
		return nil, err
	}

	return &data.Response{
		Error:   false,
		Message: "success",
		Buffer:  b,
	}, nil
}

func (o *RepositoryService) EditBranchProtection(owner string, repo string, name string, option *data.EditBranchProtectionOption) (*data.Response, error) {
	b, _, err := o.repository.EditBranchProtection(owner, repo, name, gitea.EditBranchProtectionOption{
		EnablePush:                    option.EnablePush,
		EnablePushWhitelist:           option.EnablePushWhitelist,
		PushWhitelistUsernames:        option.PushWhitelistUsernames,
		PushWhitelistTeams:            option.PushWhitelistTeams,
		PushWhitelistDeployKeys:       option.PushWhitelistDeployKeys,
		EnableMergeWhitelist:          option.EnableMergeWhitelist,
		MergeWhitelistUsernames:       option.MergeWhitelistUsernames,
		MergeWhitelistTeams:           option.MergeWhitelistTeams,
		EnableStatusCheck:             option.EnableStatusCheck,
		StatusCheckContexts:           option.StatusCheckContexts,
		RequiredApprovals:             option.RequiredApprovals,
		EnableApprovalsWhitelist:      option.EnableApprovalsWhitelist,
		ApprovalsWhitelistUsernames:   option.ApprovalsWhitelistUsernames,
		ApprovalsWhitelistTeams:       option.ApprovalsWhitelistTeams,
		BlockOnRejectedReviews:        option.BlockOnRejectedReviews,
		BlockOnOfficialReviewRequests: option.BlockOnOfficialReviewRequests,
		BlockOnOutdatedBranch:         option.BlockOnOutdatedBranch,
		DismissStaleApprovals:         option.DismissStaleApprovals,
		RequireSignedCommits:          option.RequireSignedCommits,
		ProtectedFilePatterns:         option.ProtectedFilePatterns,
	})

	if err != nil {
		return nil, err
	}

	return &data.Response{
		Error:   false,
		Message: "success",
		Buffer:  b,
	}, nil
}

func (o *RepositoryService) DeleteBranchProtection(owner, repo, name string) (*data.Response, error) {
	_, _, err := o.repository.DeleteBranch(owner, repo, name)

	if err != nil {
		return nil, err
	}

	return &data.Response{
		Error:   false,
		Message: "success",
	}, nil
}

func (o *RepositoryService) AddCollaboratorToRepository(user string, repo string, collaborator string, option *data.AddCollaboratorOption) error {
	_, err := o.repository.AddCollaboratorToRepository(user, repo, collaborator, gitea.AddCollaboratorOption{
		Permission: (*gitea.AccessMode)(option.Permission),
	})

	if err != nil {
		return err
	}

	return nil
}

func (o *RepositoryService) DeleteCollaboratorFromRepository(user string, repo string, collaborator string) error {
	_, err := o.repository.DeleteCollaboratorFromRepository(user, repo, collaborator)

	if err != nil {
		return err
	}

	return nil
}
