package service

import (
	"net/http"
	"shodo/repomanager/internal/data"
	"shodo/repomanager/internal/gateway"

	"code.gitea.io/sdk/gitea"
)

type OrganizationService struct {
	repository gateway.IOrganizationGateway
}

func NewOrganizationService() *OrganizationService {
	return &OrganizationService{
		repository: gateway.NewOrganizationGateway(),
	}
}

func (o *OrganizationService) CreateOrganization(option *data.CreateOrgOption) (*data.Response, error) {
	org, _, err := o.repository.CreateOrganization(gitea.CreateOrgOption{
		Name:                      option.Name,
		FullName:                  option.FullName,
		Description:               option.Description,
		Website:                   option.Website,
		Location:                  option.Location,
		Visibility:                gitea.VisibleType(option.Visibility),
		RepoAdminChangeTeamAccess: option.RepoAdminChangeTeamAccess,
	})

	if err != nil {
		return nil, err
	}

	return &data.Response{
		Error:   false,
		Message: "success",
		Buffer:  org,
	}, nil
}

func (o *OrganizationService) GetOrganization(orgName string) (*data.Response, error) {
	org, _, err := o.repository.GetOrganization(orgName)

	if err != nil {
		return nil, err
	}

	return &data.Response{
		Error:   false,
		Message: "success",
		Buffer:  org,
	}, nil
}

func (o *OrganizationService) EditOrganization(orgName string, option *data.EditOrgOption) (*http.Response, error) {
	r, err := o.repository.EditOrganization(orgName, gitea.EditOrgOption{
		FullName:    option.FullName,
		Description: option.Description,
		Website:     option.Website,
		Location:    option.Location,
		Visibility:  gitea.VisibleType(option.Visibility),
	})

	if err != nil {
		return nil, err
	}

	return r.Response, nil
}

func (o *OrganizationService) DeleteOrganization(orgName string) (*http.Response, error) {
	r, err := o.repository.DeleteOrganization(orgName)
	if err != nil {
		return nil, err
	}

	return r.Response, nil
}

func (o *OrganizationService) ListOrganizationMembers(orgName string, option *data.ListOrgMembershipOption) (*data.Response, error) {
	users, _, err := o.repository.ListOrganizationMembers(orgName, gitea.ListOrgMembershipOption{
		ListOptions: gitea.ListOptions{
			Page:     option.Page,
			PageSize: option.PageSize,
		},
	})

	if err != nil {
		return nil, err
	}

	return &data.Response{
		Error:   false,
		Message: "success",
		Buffer:  users,
	}, nil
}

func (o *OrganizationService) CreateRepositoryInOrganization(orgName string, option *data.CreateRepoOption) (*data.Response, error) {
	repo, r, err := o.repository.CreateRepositoryInOrganization(orgName, gitea.CreateRepoOption{
		Name:          option.Name,
		Description:   option.Description,
		Private:       option.Private,
		IssueLabels:   option.IssueLabels,
		AutoInit:      option.AutoInit,
		Template:      option.Template,
		Gitignores:    option.Gitignores,
		License:       option.License,
		Readme:        option.Readme,
		DefaultBranch: option.DefaultBranch,
		TrustModel:    gitea.TrustModel(option.TrustModel),
	})

	if err != nil {
		return nil, err
	}

	return &data.Response{
		Error:   false,
		Message: r.Status,
		Buffer:  repo,
	}, nil
}

func (o *OrganizationService) CreateTeam(orgName string, option *data.CreateTeamOption) (*data.Response, error) {
	team, r, err := o.repository.CreateTeam(orgName, gitea.CreateTeamOption{
		Name:                    option.Name,
		Description:             option.Description,
		Permission:              gitea.AccessMode(option.Permission),
		CanCreateOrgRepo:        option.CanCreateOrgRepo,
		IncludesAllRepositories: option.IncludesAllRepositories,
	})

	if err != nil {
		return nil, err
	}

	return &data.Response{
		Error:   false,
		Message: r.Status,
		Buffer:  team,
	}, nil
}

func (o *OrganizationService) DeleteTeam(teamId int64) (*http.Response, error) {
	r, err := o.repository.DeleteTeam(teamId)

	if err != nil {
		return nil, err
	}

	return r.Response, nil
}

func (o *OrganizationService) AddTeamMember(id int64, user string) (*http.Response, error) {
	r, err := o.repository.AddTeamMember(id, user)

	if err != nil {
		return nil, err
	}

	return r.Response, nil
}

func (o *OrganizationService) RemoveTeamMember(id int64, user string) (*http.Response, error) {
	r, err := o.repository.RemoveTeamMember(id, user)

	if err != nil {
		return nil, err
	}

	return r.Response, nil
}

func (o *OrganizationService) AddRepositoryToTeam(id int64, org, repo string) (*http.Response, error) {
	r, err := o.repository.AddRepositoryToTeam(id, org, repo)

	if err != nil {
		return nil, err
	}

	return r.Response, nil
}
