package handler

import (
	"net/http"
	"shodo/repomanager/internal/handler/rest"

	"github.com/go-chi/chi/v5"
)

func Routes() http.Handler {

	r := chi.NewRouter()
	r.Route("/api/organization", func(r chi.Router) {
		r.Get("/{org_name}", rest.GetOrganizationHandler)
		r.Get("/members/{org_name}", rest.ListOrganizationMembersHandler)
		r.Patch("/{org_name}", rest.EditOrganizationHandler)
		r.Delete("/{org_name}", rest.DeleteOrganizationHandler)
		r.Post("/new", rest.CreateOrganizationHandler)
		r.Post("/new/repo/{org_name}", rest.CreateRepositoryInOrganizationHandler)
		r.Put("/team/{team_id}/{user}", rest.AddTeamMemberHandler)
		r.Delete("/team/{team_id}/{user}", rest.RemoveTeamMemberHandler)
		r.Post("/new/team/{org_name}", rest.CreateTeamHandler)
		r.Delete("/team/{team_id}", rest.DeleteTeamHandler)
		r.Put("/{org_name}/team/{team_id}/repo/{repo}", rest.AddRepositoryToTeamHandler)
	})

	r.Route("/api/repository", func(r chi.Router) {
		r.Post("/create", rest.CreateRepositoryHandler)
		r.Get("/{owner}/{repo}", rest.GetRepositoryHandler)
		r.Patch("/{owner}/{repo}", rest.EditRepositoryHandler)
		r.Delete("/{owner}/{repo}", rest.DeleteRepositoryHandler)
		r.Get("/branches/{owner}/{repo}", rest.ListRepositoryBranchesHandler)
		r.Get("/branches/{owner}/{repo}/{branch}", rest.GetSpecificBranchHandler)
		r.Post("/branches/create/{owner}/{repo}", rest.CreateBranchHandler)
		r.Post("/branches/delete/{owner}/{repo}/{branch}", rest.DeleteBranchHandler)
		r.Post("/protection/create/{owner}/{repo}", rest.CreateBranchProtectionHandler)
		r.Delete("/protection/delete/{owner}/{repo}/{branch}", rest.DeleteBranchProtectionHandler)
		r.Patch("/protection/edit/{owner}/{repo}/{branch}", rest.EditBranchProtectionHandler)
		r.Post("/collaborators/create/{owner}/{repo}/{collaborator}", rest.AddCollaboratorToRepositoryHandler)
		r.Delete("/collaborators/delete/{owner}/{repo}/{collaborator}", rest.DeleteCollaboratorFromRepositoryHandler)
		r.Get("/collaborators/{owner}/{repo}", rest.ListRepositoryCollaboratorsHandler)
	})

	return r
}
