package rest

import (
	"net/http"
	"shodo/repomanager/cmd/app/utils"
	"shodo/repomanager/internal/data"
	"shodo/repomanager/internal/service"
	"strconv"

	"github.com/go-chi/chi/v5"
)

var (
	OrgService *service.OrganizationService
)

func CreateOrganizationHandler(w http.ResponseWriter, r *http.Request) {
	var org data.CreateOrgOption
	err := utils.ReadJson(w, r, &org)

	if err != nil {
		utils.ErrorJson(w, err.Error(), http.StatusBadRequest)
		return
	}

	response, err := OrgService.CreateOrganization(&org)

	if err != nil {
		utils.ErrorJson(w, err.Error(), http.StatusBadRequest)
		return
	}

	utils.WriteJson(w, http.StatusCreated, response)
}

func GetOrganizationHandler(w http.ResponseWriter, r *http.Request) {
	org := chi.URLParam(r, "org_name")

	response, err := OrgService.GetOrganization(org)

	if err != nil {
		utils.ErrorJson(w, err.Error(), http.StatusBadRequest)
		return
	}

	utils.WriteJson(w, http.StatusOK, response)
}

func EditOrganizationHandler(w http.ResponseWriter, r *http.Request) {
	org := chi.URLParam(r, "org_name")
	var orgDetais data.EditOrgOption

	err := utils.ReadJson(w, r, &orgDetais)

	if err != nil {
		utils.ErrorJson(w, err.Error(), http.StatusBadRequest)
		return
	}

	response, err := OrgService.EditOrganization(org, &orgDetais)

	if err != nil {
		utils.ErrorJson(w, err.Error(), http.StatusBadRequest)
		return
	}

	utils.WriteHeader(w, response.StatusCode)
}

func DeleteOrganizationHandler(w http.ResponseWriter, r *http.Request) {
	response, err := OrgService.DeleteOrganization(chi.URLParam(r, "org_name"))

	if err != nil {
		utils.ErrorJson(w, err.Error(), http.StatusBadRequest)
		return
	}

	utils.WriteHeader(w, response.StatusCode)
}

func ListOrganizationMembersHandler(w http.ResponseWriter, r *http.Request) {
	page, _ := strconv.Atoi(r.URL.Query().Get("page"))
	limit, _ := strconv.Atoi(r.URL.Query().Get("limit"))

	response, err := OrgService.ListOrganizationMembers(chi.URLParam(r, "org_name"), &data.ListOrgMembershipOption{
		ListOptions: data.ListOptions{
			Page:     page,
			PageSize: limit,
		},
	})

	if err != nil {
		utils.ErrorJson(w, err.Error(), http.StatusBadRequest)
		return
	}

	utils.WriteJson(w, http.StatusOK, response)
}

func CreateRepositoryInOrganizationHandler(w http.ResponseWriter, r *http.Request) {
	var repo data.CreateRepoOption
	err := utils.ReadJson(w, r, &repo)

	if err != nil {
		utils.ErrorJson(w, err.Error(), http.StatusBadRequest)
		return
	}

	response, err := OrgService.CreateRepositoryInOrganization(chi.URLParam(r, "org_name"), &repo)

	if err != nil {
		utils.ErrorJson(w, err.Error(), http.StatusBadRequest)
		return
	}

	utils.WriteJson(w, http.StatusCreated, response)
}

func CreateTeamHandler(w http.ResponseWriter, r *http.Request) {
	org := chi.URLParam(r, "org_name")
	var team data.CreateTeamOption

	err := utils.ReadJson(w, r, &team)

	if err != nil {
		utils.ErrorJson(w, err.Error(), http.StatusBadRequest)
		return
	}

	response, err := OrgService.CreateTeam(org, &team)

	if err != nil {
		utils.ErrorJson(w, err.Error(), http.StatusBadRequest)
		return
	}

	utils.WriteJson(w, http.StatusCreated, response)
}

func DeleteTeamHandler(w http.ResponseWriter, r *http.Request) {
	teamId, _ := strconv.Atoi(chi.URLParam(r, "team_id"))

	response, err := OrgService.DeleteTeam(int64(teamId))

	if err != nil {
		utils.ErrorJson(w, err.Error(), http.StatusBadRequest)
		return
	}

	utils.WriteHeader(w, response.StatusCode)
}

func AddTeamMemberHandler(w http.ResponseWriter, r *http.Request) {
	teamId, _ := strconv.ParseInt(chi.URLParam(r, "team_id"), 10, 64)
	member := chi.URLParam(r, "user")

	response, err := OrgService.AddTeamMember(teamId, member)

	if err != nil {
		utils.ErrorJson(w, err.Error(), http.StatusBadRequest)
		return
	}

	utils.WriteHeader(w, response.StatusCode)
}

func RemoveTeamMemberHandler(w http.ResponseWriter, r *http.Request) {
	teamId, _ := strconv.ParseInt(chi.URLParam(r, "team_id"), 10, 64)
	member := chi.URLParam(r, "user")

	response, err := OrgService.RemoveTeamMember(teamId, member)

	if err != nil {
		utils.ErrorJson(w, err.Error(), http.StatusBadRequest)
		return
	}

	utils.WriteHeader(w, response.StatusCode)
}

func AddRepositoryToTeamHandler(w http.ResponseWriter, r *http.Request) {
	teamId, _ := strconv.ParseInt(chi.URLParam(r, "team_id"), 10, 64)
	org := chi.URLParam(r, "org_name")
	repo := chi.URLParam(r, "repo")

	response, err := OrgService.AddRepositoryToTeam(teamId, org, repo)

	if err != nil {
		utils.ErrorJson(w, err.Error(), http.StatusBadRequest)
		return
	}

	utils.WriteHeader(w, response.StatusCode)
}
