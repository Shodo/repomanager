package rest

import (
	"net/http"
	"shodo/repomanager/cmd/app/utils"
	"shodo/repomanager/internal/data"
	"shodo/repomanager/internal/service"
	"strconv"

	"github.com/go-chi/chi/v5"
)

var (
	RepoService *service.RepositoryService
)

func CreateRepositoryHandler(w http.ResponseWriter, r *http.Request) {
	var repo data.CreateRepoOption
	err := utils.ReadJson(w, r, &repo)

	if err != nil {
		utils.ErrorJson(w, err.Error(), http.StatusBadRequest)
		return
	}

	response, err := RepoService.CreateRepository(&repo)

	if err != nil {
		utils.ErrorJson(w, err.Error(), http.StatusBadRequest)
		return
	}

	utils.WriteJson(w, http.StatusCreated, response)
}

func GetRepositoryHandler(w http.ResponseWriter, r *http.Request) {
	owner := chi.URLParam(r, "owner")
	repo := chi.URLParam(r, "repo")

	response, err := RepoService.GetRepository(owner, repo)

	if err != nil {
		utils.ErrorJson(w, err.Error(), http.StatusBadRequest)
		return
	}

	utils.WriteJson(w, http.StatusOK, response)
}

func EditRepositoryHandler(w http.ResponseWriter, r *http.Request) {
	owner := chi.URLParam(r, "owner")
	repo := chi.URLParam(r, "repo")
	var repoDetais data.EditRepoOption

	err := utils.ReadJson(w, r, &repoDetais)

	if err != nil {
		utils.ErrorJson(w, err.Error(), http.StatusBadRequest)
		return
	}

	response, err := RepoService.EditRepository(owner, repo, &repoDetais)

	if err != nil {
		utils.ErrorJson(w, err.Error(), http.StatusBadRequest)
		return
	}

	utils.WriteJson(w, http.StatusOK, response)
}

func DeleteRepositoryHandler(w http.ResponseWriter, r *http.Request) {
	owner := chi.URLParam(r, "owner")
	repo := chi.URLParam(r, "repo")
	response, err := RepoService.DeleteRepository(owner, repo)

	if err != nil {
		utils.ErrorJson(w, err.Error(), http.StatusBadRequest)
		return
	}

	utils.WriteHeader(w, response.StatusCode)
}

func ListRepositoryBranchesHandler(w http.ResponseWriter, r *http.Request) {
	owner := chi.URLParam(r, "owner")
	repo := chi.URLParam(r, "repo")
	page, _ := strconv.Atoi(r.URL.Query().Get("page"))
	limit, _ := strconv.Atoi(r.URL.Query().Get("limit"))

	response, err := RepoService.ListRepositoryBranches(owner, repo, &data.ListRepoBranchesOptions{
		ListOptions: data.ListOptions{
			Page:     page,
			PageSize: limit,
		},
	})

	if err != nil {
		utils.ErrorJson(w, err.Error(), http.StatusBadRequest)
		return
	}

	utils.WriteJson(w, http.StatusOK, response)
}

func GetSpecificBranchHandler(w http.ResponseWriter, r *http.Request) {
	owner := chi.URLParam(r, "owner")
	repo := chi.URLParam(r, "repo")
	branch := chi.URLParam(r, "branch")

	response, err := RepoService.GetSpecificBranch(owner, repo, branch)

	if err != nil {
		utils.ErrorJson(w, err.Error(), http.StatusBadRequest)
		return
	}

	utils.WriteJson(w, http.StatusOK, response)
}

func ListRepositoryCollaboratorsHandler(w http.ResponseWriter, r *http.Request) {
	owner := chi.URLParam(r, "owner")
	repo := chi.URLParam(r, "repo")
	page, _ := strconv.Atoi(r.URL.Query().Get("page"))
	limit, _ := strconv.Atoi(r.URL.Query().Get("limit"))

	response, err := RepoService.ListRepositoryCollaborators(owner, repo, &data.ListCollaboratorsOptions{
		ListOptions: data.ListOptions{
			Page:     page,
			PageSize: limit,
		},
	})

	if err != nil {
		utils.ErrorJson(w, err.Error(), http.StatusBadRequest)
		return
	}

	utils.WriteJson(w, http.StatusOK, response)
}

func CreateBranchHandler(w http.ResponseWriter, r *http.Request) {
	owner := chi.URLParam(r, "owner")
	repo := chi.URLParam(r, "repo")
	var branchOptions data.CreateBranchOption

	err := utils.ReadJson(w, r, &branchOptions)

	if err != nil {
		utils.ErrorJson(w, err.Error(), http.StatusBadRequest)
		return
	}

	response, err := RepoService.CreateBranch(owner, repo, &branchOptions)

	if err != nil {
		utils.ErrorJson(w, err.Error(), http.StatusBadRequest)
		return
	}

	utils.WriteJson(w, http.StatusCreated, response)
}

func DeleteBranchHandler(w http.ResponseWriter, r *http.Request) {
	owner := chi.URLParam(r, "owner")
	repo := chi.URLParam(r, "repo")
	branch := chi.URLParam(r, "branch")

	response, err := RepoService.DeleteBranch(owner, repo, branch)

	if err != nil {
		utils.ErrorJson(w, err.Error(), http.StatusBadRequest)
		return
	}

	utils.WriteJson(w, http.StatusOK, response)
}

func CreateBranchProtectionHandler(w http.ResponseWriter, r *http.Request) {
	owner := chi.URLParam(r, "owner")
	repo := chi.URLParam(r, "repo")

	var branchProtectionOptions data.CreateBranchProtectionOption

	err := utils.ReadJson(w, r, &branchProtectionOptions)

	if err != nil {
		utils.ErrorJson(w, err.Error(), http.StatusBadRequest)
		return
	}

	response, err := RepoService.CreateBranchProtection(owner, repo, &branchProtectionOptions)

	if err != nil {
		utils.ErrorJson(w, err.Error(), http.StatusBadRequest)
		return
	}

	utils.WriteJson(w, http.StatusCreated, response)
}

func EditBranchProtectionHandler(w http.ResponseWriter, r *http.Request) {
	owner := chi.URLParam(r, "owner")
	repo := chi.URLParam(r, "repo")
	name := chi.URLParam(r, "name")

	var branchProtectionOptions data.EditBranchProtectionOption

	err := utils.ReadJson(w, r, &branchProtectionOptions)

	if err != nil {
		utils.ErrorJson(w, err.Error(), http.StatusBadRequest)
		return
	}

	response, err := RepoService.EditBranchProtection(owner, repo, name, &branchProtectionOptions)

	if err != nil {
		utils.ErrorJson(w, err.Error(), http.StatusBadRequest)
		return
	}

	utils.WriteJson(w, http.StatusOK, response)
}

func DeleteBranchProtectionHandler(w http.ResponseWriter, r *http.Request) {
	owner := chi.URLParam(r, "owner")
	repo := chi.URLParam(r, "repo")
	name := chi.URLParam(r, "name")

	response, err := RepoService.DeleteBranchProtection(owner, repo, name)

	if err != nil {
		utils.ErrorJson(w, err.Error(), http.StatusBadRequest)
		return
	}

	utils.WriteJson(w, http.StatusOK, response)
}

func AddCollaboratorToRepositoryHandler(w http.ResponseWriter, r *http.Request) {
	owner := chi.URLParam(r, "owner")
	repo := chi.URLParam(r, "repo")
	collaborator := chi.URLParam(r, "collaborator")

	var collaboratorOptions data.AddCollaboratorOption

	err := utils.ReadJson(w, r, &collaboratorOptions)

	if err != nil {
		utils.ErrorJson(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = RepoService.AddCollaboratorToRepository(owner, repo, collaborator, &collaboratorOptions)

	if err != nil {
		utils.ErrorJson(w, err.Error(), http.StatusBadRequest)
		return
	}

	utils.WriteHeader(w, http.StatusOK)
}

func DeleteCollaboratorFromRepositoryHandler(w http.ResponseWriter, r *http.Request) {
	owner := chi.URLParam(r, "owner")
	repo := chi.URLParam(r, "repo")
	collaborator := chi.URLParam(r, "collaborator")

	err := RepoService.DeleteCollaboratorFromRepository(owner, repo, collaborator)

	if err != nil {
		utils.ErrorJson(w, err.Error(), http.StatusBadRequest)
		return
	}

	utils.WriteHeader(w, http.StatusOK)
}
