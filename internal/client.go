package internal

import (
	"context"
	"time"

	"code.gitea.io/sdk/gitea"
)

var giteaClient *gitea.Client

func CreateTimeOutForRequests() (context.Context, context.CancelFunc) {
	return context.WithTimeout(context.Background(), time.Duration(5*time.Second))
}

func PointToGiteaClientInstance(c *gitea.Client) {

	if giteaClient == nil {
		giteaClient = c
	} else {
		return
	}

}

func GetGiteaClientInstance() *gitea.Client {
	return giteaClient
}
