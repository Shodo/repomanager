# repomanager - A Shodo children application

Repomanager is a application under development for support Shodo projects.
## Brief

| REPOSITORY ROUTES GROUP      |
| ------------------ |
| <span style="color:red"><b>/api/repository</b></span> |

| Function          | Description                                   | Method    | Endpoint                  |
| --------------- | ------------------------------------------- | --------- | -------------------------- |
| `CreateRepository()`     | Creates a repository                       | POST       | `/create`           |
| `GetRepository()`     | Get repository                       | GET      | `/{owner}/{repo}`           |
| `EditRepository()`     | Edit a repository's properties. Only fields that are set will be changed  | PATCH       | `/{owner}/{repo}`           |
| `DeleteRepository()`     | Deletes a repository                     | DELETE    | `/{owner}/{repo}`           |
| `ListRepositoryBranches()`     | List a repository's branches                      | GET       | `/branches/{owner}/{repo}`           |
| `GetSpecificBranch()`     | Retrieve a specific branch from a repository, including its effective branch protection                      | GET       | `/branches/{owner}/{repo}/{branch}`           |
| `CreateBranch()`     | Create a branch                      | POST       | `/branches/create/{owner}/{repo}`           |
| `DeleteBranch()`     | Delete a specific branch from a repository                     | DELETE       | `/branches/delete/{owner}/{repo}/{branch}`           |
| `CreateBranchProtection()`     | Create a branch protections for a repository                     | POST       | `/protection/create/{owner}/{repo}`           |
| `EditBranchProtection()`     | Edit a branch protections for a repository. Only fields that are set will be changed                     | PATCH       | `/protection/edit/{owner}/{repo}/{branch}`           |
| `DeleteBranchProtection()`     | Delete a specific branch protection for the repository                     | DELETE       | `/protection/delete/{owner}/{repo}/{branch}`           |
| `AddCollaboratorToRepository()`     | Add a collaborator to a repository                     | POST       | `/collaborators/create/{owner}/{repo}/{collaborator}`           |
| `DeleteCollaboratorFromRepository()`     | Delete a collaborator from a repository                     | DELETE       | `/collaborators/delete/{owner}/{repo}/{collaborator}`           |
| `ListRepositoryCollaborators()`     | List a repository's collaborators                      | GET       | `/collaborators/{owner}/{repo}`           |

| ORGANIZATION ROUTES GROUP      |
| ------------------ |
| <span style="color:red"><b>/api/organization</b></span> |

| Function          | Description                                   | Method    | Endpoint                  |
| --------------- | ------------------------------------------- | --------- | -------------------------- |
| `GetOrganization()`     | Get one organization by name                      | GET       | `/{org_name}`           |
| `CreateOrganization()`     | Creates an organization                      | POST      | `/new`           |
| `EditOrganization()`     | Modify one organization by options  | PATCH       | `/{org_name}`           |
| `DeleteOrganization()`     | Deletes an organization                      | DELETE    | `/{org_name}`           |
| `ListOrganizationMembers()`     | List an organization members                      | GET       | `/members/{org_name}`           |
| `CreateRepositoryInOrganization()`     | Creates an organization repository for authenticated user                      | POST       | `/new/repo/{org_name}`           |
| `CreateTeam()`     | Creates a team for an organization                      | POST       | `/new/team/{org_name}`           |
| `DeleteTeam()`     | Deletes a team of an organization                      | DELETE       | `/team/{team_id}`           |
| `AddTeamMember()`     | Adds a member to a team                     | PUT       | `/team/{team_id}/{user}`           |
| `RemoveTeamMember()`     | Removes a member from a team                     | DELETE       | `/team/{team_id}/{user}`           |
| `AddRepositoryToTeam()`     | Adds a repository to a team                     | PUT       | `/{org_name}/team/{team_id}/repo/{repo}`           |

## HOW TO RUN

| ENVIRONMENT VAR NAME         | Value            |
| --------------- | ----------------- |
| APP_PORT            | e.g: 8080            |
| GITEA_TOKEN            | e.g: my-secret-token            |
| BINARY            | e.g: repo-api            |

Environment variables must be set before running or the application will panic and stop the execution. One way is create a file named .env or set the envvars directly on your terminal.

Also inside the root directory create a bin folder where go compiler will drop the executable using the name contained in BINARY environment variable.

Before type the command to run, ensure that you are at the source code root directory.

Needless to say that you need a go compiler version 1.21 installed. 

## Install Dependencies

go mod tidy

## Run

make run

## HOW TO STOP

make stop

## Contact

Filipe "Hamza" Cruz [filipe@shodo.dev](mailto:filipe@shodo.dev).

