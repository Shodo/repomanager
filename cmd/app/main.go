package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"shodo/repomanager/internal"
	"shodo/repomanager/internal/handler"
	"shodo/repomanager/internal/handler/rest"
	"shodo/repomanager/internal/service"

	"code.gitea.io/sdk/gitea"
	"github.com/joho/godotenv"
)

func main() {

	err := godotenv.Load()

	if err != nil {
		log.Panic("failed to load environment variables")
	}

	gtClient, err := gitea.NewClient("https://gitea.com", gitea.SetToken(os.Getenv("GITEA_TOKEN")))

	if err != nil {
		panic(err)
	}

	internal.PointToGiteaClientInstance(gtClient)

	rest.OrgService = service.NewOrganizationService()
	rest.RepoService = service.NewRepositoryService()

	err = http.ListenAndServe(fmt.Sprintf(":%s", os.Getenv("APP_PORT")), handler.Routes())

	if err != nil {
		panic(err)
	}

}
