package utils

import (
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"shodo/repomanager/internal/data"
	"strconv"
)

func WriteHeader(w http.ResponseWriter, status int) {
	w.WriteHeader(status)
}

func WriteJson(writer http.ResponseWriter, status int, buff interface{}, headers ...http.Header) error {
	buffer, err := json.Marshal(buff)

	if err != nil {
		return err
	}

	if len(headers) > 0 {
		for k, v := range headers[0] {
			writer.Header()[k] = v
		}
	}

	writer.Header().Set("Content-Type", "application/json; charset=utf-8")
	writer.Header().Set("Content-Length", strconv.Itoa(len(buffer)))
	writer.WriteHeader(status)
	_, err = writer.Write(buffer)

	if err != nil {
		return err
	}

	return nil
}

func ReadJson(writer http.ResponseWriter, request *http.Request, buff interface{}) error {
	maxBytes := 1024 * 10
	request.Body = http.MaxBytesReader(writer, request.Body, int64(maxBytes))

	dec := json.NewDecoder(request.Body)

	defer request.Body.Close()

	dec.DisallowUnknownFields()

	err := dec.Decode(buff)
	if err != nil {
		return err
	}

	err = dec.Decode(&struct{}{})
	if err != io.EOF {
		return errors.New("body must only contain a single JSON value")
	}

	return nil
}

func ErrorJson(writer http.ResponseWriter, err interface{}, status ...int) error {
	statusCode := http.StatusBadRequest

	if len(status) > 0 {
		statusCode = status[0]
	}

	return WriteJson(writer, statusCode, &data.Response{
		Error:   true,
		Message: "failed",
		Buffer:  err,
	})
}
