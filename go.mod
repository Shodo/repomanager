module shodo/repomanager

go 1.21.0

require (
	code.gitea.io/sdk/gitea v0.15.1
	github.com/go-chi/chi/v5 v5.0.10
	github.com/joho/godotenv v1.5.1
)

require github.com/hashicorp/go-version v1.2.1 // indirect
